<!DOCTYPE html>
<html>
<head>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script type="text/javascript" src ="<?= asset('js/bootstrap.min.js') ?>"></script>
	<link href="<?= asset('css/bootstrap.min.css') ?>" rel="stylesheet">		
	<meta name="author" content="Alvaro Mercado">
	<title>Laravel test Application</title>
</head>
<body>
    @section('menu')
	    @include('elements.navbar')
    @show

	<div class="container">
	    @yield('content')
	</div>
    @section('footer')
    	<footer>
    		
    	</footer>
    @show
</body>
</html>