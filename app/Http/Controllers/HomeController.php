<?php namespace App\Http\Controllers;

use App\Commands\GenerateControCommand;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->dispatch(new GenerateControCommand(2));
//		return view('home');
	}
	
	public function sendEmail(){
		Mail::send('emails.welcome', $data, function($message)
		{
			$message->from('us@example.com', 'Laravel');

			$message->to('foo@example.com')->cc('bar@example.com');

			$message->attach($pathToFile);
		});
	}

	public function getcache(){
		if (Cache::has('key'))
		{
			$variable = Cache::get('key', 'default');
				foreach ($variable as $key => $value) {
					# something
			}
		}
	}

	public function setCache(){
		# many options look http://laravel.com/docs/5.0/cache
	}
	public function databasecache(){
		Schema::create('cache', function($table)
		{
		    $table->string('key')->unique();
		    $table->text('value');
		    $table->integer('expiration');
		});
	}
}
